package it.uniroma3;

import java.util.Date;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.uniroma3.model.Allievo;

import it.uniroma3.model.Centro;
import it.uniroma3.service.AllievoService;

import it.uniroma3.model.Attivita;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Responsabile;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.AttivitaService;

import it.uniroma3.service.CentroService;
import it.uniroma3.service.ResponsabileService;

@SpringBootApplication
public class AziendaFormazioneApplication {

	@Autowired
	private CentroService centroService; 

	@Autowired
	private AllievoService allievoService;
	
	@Autowired
	private AttivitaService attivitaService;
	
	public static void main(String[] args) {
		SpringApplication.run(AziendaFormazioneApplication.class, args);
	}
	
	private Centro centro;

	@PostConstruct
	public void init() {
//		centro = new Centro("RM123", "CentroAcme", "centroacme@gmail.com", 200);
//		centroService.save(centro);
//		for(Centro c : centroService.findByCodice("RM123")) {
//			System.out.println(c.getNome());
//		}
	}
	
	
	@Autowired
	private ResponsabileService responsabileService;
	
	@PostConstruct
	public void init2() {

		Centro centro = new Centro();
		centro.setNome("Sporting Club");
		centro.setTelefono("284312");
		centro.setIndirizzo("Via della Ciocia 98");
		centro.setCodice(UUID.randomUUID().toString().toUpperCase());
		centro.setCapienzaMax(299);
		centro.setEmail("davide@gma.co");

		centroService.save(centro);
		
		
		Centro cent = new Centro();
		cent.setNome("Ciotti Badminton Club");
		cent.setTelefono("28434312");
		cent.setIndirizzo("Via della Ciociaria 98");
		cent.setCodice(UUID.randomUUID().toString().toUpperCase());
		cent.setCapienzaMax(100);
		cent.setEmail("ciotti@gmail.co");

		centroService.save(cent);

		
		Attivita attivita = new Attivita();
		attivita.setNome("Pilates");
		attivita.setDescription("Serra");
		attivita.setCodice(UUID.randomUUID().toString().toUpperCase());
		attivita.setData(new Date(1537537717));
		attivita.setCentro(cent);
		attivita.setCosto(200.0);

		attivitaService.save(attivita);
		
		Attivita attivita2 = new Attivita();
		attivita2.setNome("Sala Pesi");
		attivita2.setDescription("Serra");
		attivita2.setCodice(UUID.randomUUID().toString().toUpperCase());
		attivita2.setData(new Date(1540058400));
		attivita2.setCentro(cent);
		attivita2.setCosto(200.0);
		

		attivitaService.save(attivita2);
		
		Allievo allievo = new Allievo();
		allievo.setNome("Giulio");
		allievo.setCognome("Serra");
		allievo.setCodiceFiscale("ABC123");
		allievo.seteMail("davide@gma.co");
		allievo.setDataDiNascita(new Date(1537537717));
		allievoService.save(allievo);

		Responsabile responsab = new Responsabile();
		responsab.setNome("Giulio");
		responsab.setCognome("Serra");
		responsab.setMatricola("ABC123");
		responsab.setEmail("davide@gma.co");
		responsab.setTelefono("3456523");

		responsabileService.save(responsab);
		
		responsab.setCentroGestito(cent);
		cent.setResponsabile(responsab);
		cent.getAttivita().add(attivita);
		cent.getAttivita().add(attivita2);
		
		centroService.save(cent);
		
		responsab.setCentroGestito(centro);
		centro.setResponsabile(responsab);
		centro.getAttivita().add(attivita);
		centro.getAttivita().add(attivita2);
		
		centroService.save(centro);
		
		Date temp = new Date(1234567889);
		
		Allievo allievo1 = new Allievo("Marcin", "Czajka", "LKJMDN23JH45S123P", "PROVA@PROVA.IT", temp);
		Allievo allievo2 = new Allievo("Davide", "Zollo", "DDJMDN23JH45S123P", "PROVA1@PROVA.IT", temp);
		allievoService.save(allievo1);
		allievoService.save(allievo2);
		
		allievo.getAttivita().add(attivita);
		attivita.getAllieviPartecipanti().add(allievo);
		allievoService.save(allievo);
		attivitaService.save(attivita);


		responsabileService.save(responsab);

		
		Allievo a= allievoService.findByCodiceFiscale("ABC123");
		System.out.println(a.getNome());

	}
	

}
