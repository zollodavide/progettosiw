package it.uniroma3.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import it.uniroma3.model.Centro;
import it.uniroma3.repository.CentroRepository;

@Transactional
@Service
public class CentroService {
	
	@Autowired
	private CentroRepository centroRepository; 
	
	public Centro save(Centro centro) {
		return this.centroRepository.save(centro);
	}

	public Centro findByCodice(String codice) {
		return this.centroRepository.findByCodice(codice);
	}

	
//	public List<Centro> findByCitta(String citta){
//		List<Centro> centri = this.findAll();
//		List<Centro> temp = new ArrayList<Centro>();
//		for(Centro centro : centri){
//			if(centro.getIndirizzo().getCitta().equals(citta))
//				temp.add(centro);
//		}
//		return temp;
//	}

	public List<Centro> findAll() {
		return (List<Centro>) this.centroRepository.findAll();
	}
	
	public Centro findById(Long id) {
		Optional<Centro> centro = this.centroRepository.findById(id);
		if (centro.isPresent()) 
			return centro.get();
		else
			return null;
	}

	public boolean alreadyExists(Centro centro) {
		List<Centro> centri = this.centroRepository.findByNomeAndCodice(centro.getNome(), centro.getCodice());
		if (centri.size() > 0)
			return true;
		else 
			return false;
	}	
}
