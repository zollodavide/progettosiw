package it.uniroma3.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Responsabile;
import it.uniroma3.repository.ResponsabileRepository;

@Transactional
@Service
public class ResponsabileService {
	
	@Autowired
	private ResponsabileRepository responsabileRepository; 
	
	public Responsabile save(Responsabile centro) {
		return this.responsabileRepository.save(centro);
	}

	public Responsabile findByMatricola(String matricola) {
		System.out.println(matricola);
		Responsabile resp = this.responsabileRepository.findByMatricola(matricola);
		return resp;
	}

	public List<Responsabile> findAll() {
		return (List<Responsabile>) this.responsabileRepository.findAll();
	}
	
	public Responsabile findById(Long id) {
		Optional<Responsabile> responsabile = this.responsabileRepository.findById(id);
		if (responsabile.isPresent()) 
			return responsabile.get();
		else
			return null;
	}

	public boolean alreadyExists(Responsabile responsabile) {
		List<Responsabile> responsabili = this.responsabileRepository.findByNomeAndCognomeAndMatricola(responsabile.getNome(), responsabile.getCognome(), responsabile.getMatricola());
		if (responsabili.size() > 0)
			return true;
		else 
			return false;
	}	
}
