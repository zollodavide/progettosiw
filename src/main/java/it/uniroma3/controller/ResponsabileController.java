package it.uniroma3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import it.uniroma3.model.Responsabile;
import it.uniroma3.service.ResponsabileService;

@Controller
public class ResponsabileController {
	
	//DA SETTARE DOPO IL LOGIN
	public static String MATRICOLA = "ABC123";
	
	@Autowired
	private ResponsabileService responsabileService;
	
	@RequestMapping("/loginresp")
	public String  nuovoLogin(Model model) {
		model.addAttribute("resp", new Responsabile());
		model.addAttribute("responsabili", responsabileService.findAll());
		return "loginResponsabile";
	}
	
	
	
	@RequestMapping("/responsabile") //PER ORA DI TEST
	public String index(Model model) {
		Responsabile resp = responsabileService.findByMatricola(ResponsabileController.MATRICOLA);
		model.addAttribute("responsabile", resp);
		model.addAttribute("totPartecipanti", resp.getCentroGestito().numeroAllievi());
		return "menuResponsabile";
	}

}
