package it.uniroma3.controller.validator;

import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Allievo;

@Component
public class AllievoValidator implements Validator {

	   @Override
	    public void validate(Object o, Errors errors) {
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cognome", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "codiceFiscale", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dataDiNascita", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "eMail", "required");
	        
	        try {
	        	Date a = new Date((String) errors.getFieldValue("dataDiNascita"));
	        } catch (Exception e) {
	        	errors.rejectValue("dataDiNascita", "format");
	        }
	        
	    }

	    @Override
	    public boolean supports(Class<?> aClass) {
	        return Allievo.class.equals(aClass);
	    }	
}
