package it.uniroma3.controller;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import it.uniroma3.controller.validator.AllievoValidator;
import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.Responsabile;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.AttivitaService;
import it.uniroma3.service.ResponsabileService;

@Controller
public class AllievoController {
	
	@Autowired
	private AllievoService allievoService;
	
	@Autowired
	private AllievoValidator allievoValidator;

	@Autowired
	private ResponsabileService responsabileService;

	
    @RequestMapping("/allievi")
    public String allievi(Model model) {
        model.addAttribute("allievi", this.allievoService.findAll());
        return "allievoList";
    }
	
	@RequestMapping("/allievo")
	public String vaiForm(Model model) {
		model.addAttribute("allievo", new Allievo());
		return "formAllievo";
	}
	
	@RequestMapping(value="/nuovoAllievo", method=RequestMethod.POST)
	public String nuovoAllievo(@Valid @ModelAttribute("allievo") Allievo allievo, BindingResult bindingResult,  Model model) {
		allievoValidator.validate(allievo, bindingResult);
		
		if(!bindingResult.hasErrors()) {
			if(allievoService.alreadyExists(allievo))
				return "formAllievo"; //Già esistente
			
			allievoService.save(allievo);
			model.addAttribute("allievo", allievo);
			return "confermaAllievo"; //Prossima pagina -> SUCCESSO
		}
		else 
			return "formAllievo";
	}
	
	@RequestMapping("/confermaAllievo")
	public String allievoConfermato(Model model) {
		Responsabile resp = responsabileService.findByMatricola(ResponsabileController.MATRICOLA);
		model.addAttribute("responsabile", resp);
		model.addAttribute("totPartecipanti", resp.getCentroGestito().numeroAllievi());
		return "menuResponsabile";
	}
	
	@RequestMapping(value="/cancelAllievo/{codiceFiscale}", method=RequestMethod.GET)
	public String cancelAllievo(@PathVariable("codiceFiscale") String codiceFiscale, Model model) {
		System.out.println(codiceFiscale);
		allievoService.deleteAllievo(codiceFiscale);
		Responsabile resp = responsabileService.findByMatricola(ResponsabileController.MATRICOLA);
		model.addAttribute("responsabile", resp);
		model.addAttribute("totPartecipanti", resp.getCentroGestito().numeroAllievi());
		return "menuResponsabile";
	}
	
	@RequestMapping("/partecipazione")
	public String partecipazione(Model model) {
		model.addAttribute("allievo", new Allievo());
		return "nuovaPartecipazioneAllievo";
	}
	
	@Autowired
	private AttivitaService attivitaService;
	
	@RequestMapping("/cercaAllievo")
	public String cercaAllievo( @ModelAttribute("allievo") Allievo allievo ,Model model) {
		Allievo part = allievoService.findByCodiceFiscale(allievo.getCodiceFiscale());
		
		if(part==null) {
			model.addAttribute("allievo", new Allievo());
			model.addAttribute("exists", "Non esiste un Allievo con questo CF.");
			return "nuovaPartecipazioneAllievo";
		}
		List<Attivita> act = attivitaService.findAll();
		act.removeAll(part.getAttivita());
		Integer count = part.getAttivita().size();
		model.addAttribute("count", count);
		model.addAttribute("allievo", part);
		model.addAttribute("activity", act);
		return "selezioneAttivitaPartecipazione";
	}
	
	
	@RequestMapping(value="/confermaNuovaPartecipazione/{codiceFiscale}")
	public String nuovaPartecipazione( @ModelAttribute("allievo") Allievo allievo,@PathVariable("codiceFiscale") String codiceFiscale ,Model model) {
		String cod = allievo.getAttivita().get(allievo.getAttivita().size()-1).getCodice();
		Attivita act = attivitaService.findByCodice(cod);
		allievo = allievoService.findByCodiceFiscale(codiceFiscale); 
		allievo.getAttivita().add(act);
		allievoService.save(allievo);
		act.getAllieviPartecipanti().add(allievo);
		attivitaService.save(act);
		Responsabile resp = responsabileService.findByMatricola(ResponsabileController.MATRICOLA);
		model.addAttribute("responsabile", resp);
		model.addAttribute("totPartecipanti", resp.getCentroGestito().numeroAllievi());
		return "menuResponsabile";
	}
	
    @RequestMapping(value = "/allievo/{id}", method = RequestMethod.GET)
    public String getAllievo(@PathVariable("id") Long id, Model model) {
        model.addAttribute("allievo", this.allievoService.findById(id));
    	return "userProfile";
    }
	
	

}
