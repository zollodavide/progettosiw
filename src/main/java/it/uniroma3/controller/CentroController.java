package it.uniroma3.controller;

import java.util.ArrayList;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.controller.validator.*;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Responsabile;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.ResponsabileService;

@Controller
public class CentroController {

	@Autowired
	private CentroService centroService;
	
	@Autowired
	private CentroValidator centroValidator;
	
	@RequestMapping("/centro")
	public String nuovoCentro(Model model) {
		model.addAttribute("centro", new Centro());
		return "formCentro";
	}
	
	@Autowired
	private ResponsabileService responsabileService;
	

	
	
	@RequestMapping("/")
	public String log(Model model) {
		return "index";
	}
	
	@RequestMapping("/menu")
	public String direttoreIndietro(Model model) {
		model.addAttribute("centri", centroService.findAll());
		return "menuDirettore";
	}
	
	@RequestMapping("/visualizzaCentro/{codice}")
	public String vediCentro(@PathVariable("codice") String codice, Model model) {
		Centro centro = centroService.findByCodice(codice);
		model.addAttribute("centro", centro);
		return "infoCentro";
		
	}
	
	
	@RequestMapping(value="/nuovoCentro",  method=RequestMethod.POST)
	public String centro(@Valid @ModelAttribute("centro") Centro centro, BindingResult bindingResult, Model model) {
		centroValidator.validate(centro, bindingResult);
		
		if(!bindingResult.hasErrors() ) {
			if(centroService.alreadyExists(centro)) {
				model.addAttribute("exists", true);
				return "formCentro";
			}
			Responsabile resp = responsabileService.findByMatricola(centro.getResponsabile().getMatricola());
			if(resp == null)
				return "formCentro";
			
			resp.setCentroGestito(centro);
			centro.setResponsabile(resp);
			centro.setCodice(UUID.randomUUID().toString().toUpperCase());
			model.addAttribute("centro", centro);
			centroService.save(centro);
			responsabileService.save(resp);
			model.addAttribute("centri", centroService.findAll());
			return "menuDirettore";		
		}
		return "formCentro";
	}
	
	
	@RequestMapping("/nuovaAttivita")
	public String attivita(Model model) {
		model.addAttribute("centri", centroService.findAll());
		model.addAttribute("cent", new Centro());
		return "centroNuovaAttivita";
	}
	
	
	
	
}
