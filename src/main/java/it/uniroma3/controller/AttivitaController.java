package it.uniroma3.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import it.uniroma3.controller.validator.AttivitaValidator;
import it.uniroma3.model.*;
import it.uniroma3.service.AttivitaService;
import it.uniroma3.service.CentroService;

@Controller
public class AttivitaController {
	
	@Autowired
	private AttivitaService attivitaService;
	
	@Autowired
	private AttivitaValidator attivitaValidator;
	
	@Autowired
	private CentroService centroService;
	
	@RequestMapping("/newAttivita")
	public String nuovaAttivita(@ModelAttribute("cent") Centro centre, Model model) {
		centre = centroService.findByCodice(centre.getCodice());
		Attivita att =new Attivita();
		model.addAttribute("attivita", att);
		model.addAttribute("centro", centre);
		return "formAttivita";
	}
	
	@RequestMapping("/attivita/{codiceCentro}")
	public String Attivita(@PathVariable("codiceCentro") String codice,@Valid @ModelAttribute("attivita") Attivita attivita, BindingResult bindingResult, Model model) {
		Centro centro = centroService.findByCodice(codice);
		this.attivitaValidator.validate(attivita, bindingResult);
		
		if(bindingResult.hasErrors()) {
			model.addAttribute("centro", centro);
			return "formAttivita";
		}
		
		centro.getAttivita().add(attivita);
		attivita.setCentro(centro);
		
		attivita.setCodice(UUID.randomUUID().toString().toUpperCase());
		
		attivitaService.save(attivita);
		centroService.save(centro);
		
		model.addAttribute("attivita", attivitaService.findAll());
		return "listaAttivita";
	}
	
	@RequestMapping("/menuAttivita")
	public String direttoreIndietro(Model model) {
		model.addAttribute("attivita", attivitaService.findAll());
		return "listaAttivita";
	}
	
	@RequestMapping("/allievoList/{codice}")
	public String listaAllievi(@PathVariable("codice") String codice, Model model) {
		Attivita attivita = attivitaService.findByCodice(codice);
		model.addAttribute("allievi", attivita.getAllieviPartecipanti());
		return "allievoList";
	}
	

}
