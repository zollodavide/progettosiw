package it.uniroma3.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Attivita {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private String codice;
	
	@Column(nullable = false)
	private String nome;
	
	@Column(length = 2000)
	private String descrizione;
	
	@Column(nullable = false)
	@Temporal (TemporalType.DATE)
	private Date data;
	
	@Column(nullable=false)
	private Double costo;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Centro centro;
	
	@ManyToMany (mappedBy="attivita")
	private List<Allievo> allieviPartecipanti;
	
	
	private String ora;

	public Attivita(String codice, String nome, Date data, Date ora) {
		super();
		this.codice = codice;
		this.nome = nome;
		this.data = data;
		this.allieviPartecipanti = new ArrayList<Allievo>();
		
	}
	
	public Attivita() {
		this.allieviPartecipanti = new ArrayList<Allievo>();
	}
	

	public Centro getCentro() {
		return centro;
	}



	public void setCentro(Centro centro) {
		this.centro = centro;
	}



	public List<Allievo> getAllieviPartecipanti() {
		return allieviPartecipanti;
	}



	public void setAllieviPartecipanti(List<Allievo> allieviPartecipanti) {
		this.allieviPartecipanti = allieviPartecipanti;
	}



	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescription() {
		return descrizione;
	}

	public void setDescription(String description) {
		this.descrizione = description;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Double getCosto() {
		return costo;
	}



	public void setCosto(Double costo) {
		this.costo = costo;
	}



	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@SuppressWarnings("deprecation")
	public String getOra() {
		Calendar a = GregorianCalendar.getInstance();
		a.setTime(this.data);

//		String s = Integer.toString(a.HOUR_OF_DAY) + " " + Integer.toString(this.data.getMinutes());
		return Integer.toString(a.HOUR) + ":" + Integer.toString(a.MINUTE);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codice == null) ? 0 : codice.hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Attivita other = (Attivita) obj;
		if (codice == null) {
			if (other.codice != null)
				return false;
		} else if (!codice.equals(other.codice))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Attivita [codice=" + codice + ", nome=" + nome + ", description=" + descrizione + ", data=" + data
				+ ", centro=" + centro + "]";
	}


	
}
