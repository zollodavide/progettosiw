package it.uniroma3.model;

import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
//@NamedQuery(name = "findAllCentri", query = "SELECT c from CENTRO c")
public class Centro {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private String codice;
	
	@Column(nullable = false)
	private String nome;

	@Column(nullable = false)
	private String email;
	
	@Column(nullable = false)
	private Integer capienzaMax;
	
	@Column
	private String indirizzo;
	
	@OneToOne(fetch=FetchType.EAGER)
	private Responsabile responsabile;
	
	private Double incasso;


	public Double getIncasso() {
		
		double tot = 0;
		
		for(Attivita att : this.attivita) {
			double costo = att.getCosto();
			tot += (costo*att.getAllieviPartecipanti().size());
		}
		
		return tot;
	}


	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name = "centro_id")
	private List<Attivita> attivita;
	
	@Column
	private String telefono;

	public Centro(String codice, String nome, String email, Integer capienzaMax) {
		super();
		this.codice = codice;
		this.nome = nome;
		this.email = email;
		this.capienzaMax = capienzaMax;
		this.attivita=new ArrayList<Attivita>();
	}
	
	public Centro() {
		this.attivita=new ArrayList<Attivita>();
	}

	public Responsabile getResponsabile() {
		return responsabile;
	}

	public void setResponsabile(Responsabile responsabile) {
		this.responsabile = responsabile;
	}

	public Long getId() {
		return id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getCapienzaMax() {
		return capienzaMax;
	}

	public void setCapienzaMax(Integer capienzaMax) {
		this.capienzaMax = capienzaMax;
	}

	public String getIndirizzo() {
		return this.indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<Attivita> getAttivita() {
		return attivita;
	}

	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codice == null) ? 0 : codice.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((indirizzo == null) ? 0 : indirizzo.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Centro other = (Centro) obj;
		if (codice == null) {
			if (other.codice != null)
				return false;
		} else if (!codice.equals(other.codice))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (indirizzo == null) {
			if (other.indirizzo != null)
				return false;
		} else if (!indirizzo.equals(other.indirizzo))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	
	
	public int numeroAllievi() {
		int tot= 0;
		for (Attivita attivita : this.attivita) {
			tot += attivita.getAllieviPartecipanti().size();
			System.out.println("ECCOLO: "+ tot);
		}
		return tot;
	}
	
}
