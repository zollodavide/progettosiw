package it.uniroma3.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Responsabile;

public interface ResponsabileRepository extends CrudRepository<Responsabile, Long> {

	public Responsabile findByMatricola(String matricola);

	public List<Responsabile> findByNomeAndCognome(String nome, String cognome);
	
	public List<Responsabile> findByNomeAndCognomeAndMatricola(String nome,String cognome, String matricola);

}